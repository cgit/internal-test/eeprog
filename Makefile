# ----------------------------------------------------------------------------
# Makefile for building eeprog
#
# Copyright 2018 ISEE (http://www.isee.biz/)
#

ifndef DESTDIR
DESTDIR			   ?= /tmp/isee/
endif

CFLAGS				= -Wall -O2
CC					= arm-linux-gnueabihf-gcc
INSTALL				= install
TARGET				= eeprog


all: $(TARGET)

eeprog: eeprog.o 24cXX.o
	$(CC) $(CFLAGS) $^ -o $@

install: $(TARGET)
	$(INSTALL) $^ $(DESTDIR)/usr/bin

clean distclean:
	rm -rf *.o $(TARGET)


# ----------------------------------------------------------------------------

.PHONY: $(PHONY) install clean distclean

# End of file
# vim: syntax=make